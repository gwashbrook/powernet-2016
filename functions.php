<?php
/**
 * Powernet
 *
 * This file adds functions to the powernet theme.
 *
 * @package Powernet
 * @author  Graham Washbrook
 * @link    https://powerhut.net/themes/
 */

//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Setup Theme
// include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );

//* Set Localization (do not remove)
load_child_theme_textdomain( 'pnet2016', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'pnet' ) );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', __( 'Powernet Theme', 'pnet2016' ) );
define( 'CHILD_THEME_URL', 'https://powerhut.net/themes/' );
define( 'CHILD_THEME_VERSION', '1.0.19' );

//* Enqueue Scripts and Styles
add_action( 'wp_enqueue_scripts', 'child_enqueue_scripts_styles' );
function child_enqueue_scripts_styles() {

	// Theme js
	wp_enqueue_script( 'pnet', get_bloginfo( 'stylesheet_directory' ) . '/js/pnet.js', array( 'jquery' ), CHILD_THEME_VERSION );

	// Google fonts
	// wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Advent+Pro:300,400|Roboto+Slab:400,300|Oxygen:300,400', array(), CHILD_THEME_VERSION );
	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Oxygen:300,400', array(), CHILD_THEME_VERSION );

	// Font Awesome
	wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css', array(), CHILD_THEME_VERSION );
	
}

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

//* Add Accessibility support
add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Allow plugins and themes to manage the document title tag. Since WP 4.1
add_theme_support( 'title-tag' );

//* Add support footer widgets
add_theme_support( 'genesis-footer-widgets', 3 );

//* Add support for structural wraps
add_theme_support( 'genesis-structural-wraps', array(
	'header',
	'site-tagline',
	'nav',
	'subnav',
	'home-featured',
	'site-inner',
	'before-footer-widgets',
	'footer-widgets',
	'footer'
) );


//* Add support for custom header
add_theme_support( 'custom-header', array(
	'header-text'         => false,
	'width'               => 177,
	'height'              => 60,
	'header-selector'     => '.site-title a',

) );


//* Add support for custom background with callback
add_theme_support( 'custom-background', array(
	'wp-head-callback' => 'child_background_callback'
) ); 


//* Custom background colour callback
function child_background_callback() {
	if ( ! get_background_color() )
		return;
	printf( '<style>body { background-color: #%s; }</style>' . "\n", get_background_color() );
}


//* Add new image sizes
add_image_size( 'single-cover', 848, 477, array('left','top')); // 16:9


//* Remove secondary sidebar and layouts
unregister_sidebar( 'sidebar-alt' );
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );
genesis_unregister_layout( 'sidebar-content-sidebar' );


//* Reposition the primary navigation menu
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_after_header', 'genesis_do_nav', 15 );


//* Reposition the secondary navigation menu
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 7 );


//* Filter secondary navigation menu args
add_filter( 'wp_nav_menu_args', 'child_secondary_menu_args' );
function child_secondary_menu_args( $args ){

	if( 'secondary' != $args['theme_location'] ) {
		return $args;
	}

	$args['depth'] = 1;
	return $args;
}


//* Remove site description from header
remove_action( 'genesis_site_description', 'genesis_seo_site_description' );
// Better to add screen reader class?
// add_filter( 'genesis_attr_site-description', 'genesis_attributes_screen_reader_class' );
// add_filter( 'genesis_attr_site-tagline', 'genesis_attributes_screen_reader_class' );

//* Add the site tagline section
add_action( 'genesis_after_header', 'child_site_tagline' );
function child_site_tagline() {

	printf( '<div %s>', genesis_attr( 'site-tagline' ) );
	genesis_structural_wrap( 'site-tagline' );

		printf( '<div %s>', genesis_attr( 'site-tagline-left' ) );
		printf( '<p %s>%s</p>', genesis_attr( 'site-description' ), esc_html( get_bloginfo( 'description' ) ) );
		echo '</div>';
	
		printf( '<div %s>', genesis_attr( 'site-tagline-right' ) );
		genesis_widget_area( 'site-tagline-right' );
		echo '</div>';

	genesis_structural_wrap( 'site-tagline', 'close' );
	echo '</div>';

}


//* Hook after post widget after the entry content
add_action( 'genesis_after_entry', 'child_after_entry', 5 );
function child_after_entry() {

	if ( is_singular( 'post' ) )
		genesis_widget_area( 'after-entry', array(
			'before' => '<div class="after-entry widget-area">',
			'after'  => '</div>',
		) );

}


//* Add next/previous post navigation
add_action( 'genesis_after_entry', 'genesis_prev_next_post_nav', 9 );


//* Modify the size of the Gravatar in the author box
add_filter( 'genesis_author_box_gravatar_size', 'child_author_box_gravatar' );
function child_author_box_gravatar( $size ) {
	return 144; // wooh really?
}

//* Modify the size of the Gravatar in the entry comments
add_filter( 'genesis_comment_list_args', 'child_comments_gravatar' );
function child_comments_gravatar( $args ) {

	$args['avatar_size'] = 96;
	return $args;

}

//* Remove comment form allowed tags
add_filter( 'comment_form_defaults', 'child_remove_comment_form_allowed_tags' );
function child_remove_comment_form_allowed_tags( $defaults ) {
	
	$defaults['comment_notes_after'] = '';
	return $defaults;

}

//* Register widget areas
genesis_register_sidebar( array(
	'id'          => 'site-tagline-right',
	'name'        => __( 'Site Tagline Right', 'pnet2016' ),
	'description' => __( 'This is the site tagline right section.', 'pnet2016' ),
) );

/*
genesis_register_sidebar( array(
	'id'          => 'home-featured-1',
	'name'        => __( 'Home Featured 1', 'pnet2016' ),
	'description' => __( 'This is the home featured 1 section.', 'pnet2016' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-featured-2',
	'name'        => __( 'Home Featured 2', 'pnet2016' ),
	'description' => __( 'This is the home featured 2 section.', 'pnet2016' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-featured-3',
	'name'        => __( 'Home Featured 3', 'pnet2016' ),
	'description' => __( 'This is the home featured 3 section.', 'pnet2016' ),
) );
*/

genesis_register_sidebar( array(
	'id'          => 'after-entry',
	'name'        => __( 'After Entry', 'pnet2016' ),
	'description' => __( 'This is the after entry widget area.', 'pnet2016' ),
) );

genesis_register_sidebar( array(
	'id'          => 'before-footer-widgets-1',
	'name'        => __( 'Before Footer 1', 'pnet2016' ),
	'description' => __( 'Before Footer 1 widget area.', 'pnet2016' ),
) );
genesis_register_sidebar( array(
	'id'          => 'before-footer-widgets-2',
	'name'        => __( 'Before Footer 2', 'pnet2016' ),
	'description' => __( 'Before Footer 2 widget area.', 'pnet2016' ),
) );


//* Add before-footer-widget support if widgets are being used
add_action( 'genesis_meta', 'child_genesis_meta' );
function child_genesis_meta() {

	if ( is_active_sidebar( 'before-footer-widgets-1' ) || is_active_sidebar( 'before-footer-widgets-2' ) ) {

		//* Add Above Footer widget areas
		add_action ('genesis_before_footer','child_before_footer_widgets', 8);

	}
}

//* Add markup for before-footer-widgets
function child_before_footer_widgets() {

	printf( '<div %s>', genesis_attr( 'before-footer-widgets' ) );
	genesis_structural_wrap( 'before-footer-widgets' );

		genesis_widget_area( 'before-footer-widgets-1', array(
			'before' => '<div class="one-half first widget-area before-footer-widgets-1">',
			'after'	 => '</div>',
		) );

		genesis_widget_area( 'before-footer-widgets-2', array(
			'before' => '<div class="one-half widget-area before-footer-widgets-2">',
			'after'	 => '</div>',
		) );


	genesis_structural_wrap( 'before-footer-widgets', 'close' );
	echo '</div>'; //* end .before-footer-widgets

}



/* Add support for additional color style options
add_theme_support( 'genesis-style-selector', array(
	'theme-raspberry-frost' => __( 'Raspberry Frost', 'pnet2016' ),
	// 'theme-banana' => __( 'Banana', 'pnet2016' ),
) );
*/






/*
 * Filter wp_link_pages_args, adding <span> and .screen-reader-text class to post content navigation
 * ~ https://core.trac.wordpress.org/ticket/12158
 * ~ https://core.trac.wordpress.org/browser/tags/4.2.2/src/wp-includes/post-template.php#L0
 * ~ themes/genesis/lib/structure/post.php  genesis_do_post_content_nav()

// add_filter( 'wp_link_pages_args', 'phut_wp_link_pages_args' );
function phut_wp_link_pages_args( $params ) {
	$args = array (
		'link_before' => '<span>',
		'link_after'  => '</span>',
		'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'genesis' ) . ' </span>%',
		'separator'   => '<span class="screen-reader-text">, </span>',
	);
	$params = wp_parse_args( $args, $params );
	return $params;
}

 */


//* Move post info to before post title
add_action ('genesis_meta','child_maybe_move_post_info' );
function child_maybe_move_post_info() {
	remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
	add_action( 'genesis_entry_header', 'genesis_post_info', 5 );
}



// Remove categories and tags from entry_footer
// add_action('genesis_meta','child_maybe_remove_post_meta');
function child_maybe_remove_post_meta() {
	if ( !is_single() ) {
		remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
	}
}

// or remove the entry footer completely
// add_action('genesis_meta','child_maybe_remove_entry_footer');
function child_maybe_remove_entry_footer() {
	if (!is_single() ) {
		remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_open', 5 );
		remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
		remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_close', 15 );
	}
}


//*
add_filter ('shortcode_atts_post_categories', 'phut_shortcode_atts_post_categories' );
function phut_shortcode_atts_post_categories( $atts ) {
	$atts['before'] = '<i class="fa fa-folder-o"></i>&nbsp;&nbsp;';
	return $atts;
}


//*
add_filter ('shortcode_atts_post_tags', 'phut_shortcode_atts_post_tags' );
function phut_shortcode_atts_post_tags( $atts ) {
	$atts['before'] = '';
	$atts['sep'] = ' ';
	return $atts;
}


//* Filter the wp_tag_cloud args
add_filter('widget_tag_cloud_args','child_tag_cloud_args');
function child_tag_cloud_args( $args ) {
	$args['smallest'] = 1;
	$args['largest']  = 1;
	$args['unit']     = 'em';
	$args['order']    = 'DESC';
	$args['orderby']  = 'count';
	return $args;
}


//*
add_action ( 'genesis_before_entry', 'child_maybe_cover_featured_img' );
function child_maybe_cover_featured_img () {

	// Only for defined post types
	$post_types= array (
		'post',
		'phut_theme',
	);
	
	if( !is_singular( $post_types ) ) return;
	
	if ( has_post_thumbnail() ) {
		$cover_id = get_post_thumbnail_id( );
		$html = wp_get_attachment_image( $cover_id, 'single-cover', false, '' );
		echo '<div class="cover-wrap">' . $html . '</div>';
	}
}



//* Filter post info
add_filter( 'genesis_post_info', 'child_post_info_filter' );
function child_post_info_filter( $post_info ) {
	if( !is_page() ) {
		$post_info = '[post_date format="F j, Y"] [post_comments] [post_edit]';
	}
	return $post_info; 	
}


//* Replace Genesis search form
add_filter( 'genesis_search_form', 'child_search_form', 10, 4);
function child_search_form( $form, $search_text, $button_text, $label ) {
	if ( genesis_html5() && genesis_a11y( 'search-form' ) ) {
		$search_text = get_search_query() ? apply_filters( 'the_search_query', get_search_query() ) : apply_filters( 'genesis_search_text', __( 'Search this website', 'genesis' ) . ' &#x02026;' );
		$button_text = apply_filters( 'genesis_search_button_text', esc_attr__( 'Search', 'genesis' ) );
		$onfocus = "if ('" . esc_js( $search_text ) . "' === this.value) {this.value = '';}";
		$onblur  = "if ('' === this.value) {this.value = '" . esc_js( $search_text ) . "';}";
		// Empty label, by default. Filterable.
		$label = apply_filters( 'genesis_search_form_label', '' );
		$value_or_placeholder = ( get_search_query() == '' ) ? 'placeholder' : 'value';
		$form  = sprintf( '<form %s>', genesis_attr( 'search-form' ) );
		if ( '' == $label )  {
			$label = apply_filters( 'genesis_search_text', __( 'Search this website', 'genesis' ) );
		}
		$form_id = uniqid( 'searchform-' );
		$form .= sprintf(
			'<meta itemprop="target" content="%s"/><label class="search-form-label screen-reader-text" for="%s">%s</label><input itemprop="query-input" type="search" name="s" id="%s" %s="%s" /><button type="submit"><span class="screen-reader-text">%s</span></button></form>',
			home_url( '/?s={s}' ),
			esc_attr( $form_id ),
			esc_html( $label ),
			esc_attr( $form_id ),
			$value_or_placeholder,
			esc_attr( $search_text ),
			esc_attr( $button_text )
		);
	}
	return $form;
}


//* Filter the Genesis search text
add_filter('genesis_search_text','child_search_text');
function child_search_text( $search_text ) {
	return 'Search the blog ...';
}

//* Filter the search button text
add_filter( 'genesis_search_button_text', 'child_search_button_text' );
function child_search_button_text( $text ) {
	return 'Go'; 
}


//* Set search to posts only - clashes with WooCommerce, so beware
add_filter( 'pre_get_posts', 'child_search_filter' );
function child_search_filter( $query ) {

	if ( ( $query->is_search ) && !is_admin() ) {
		$query->set( 'post_type', array('post') );
	}
	return $query;
}


//* Replace entry content nav with stylable and accessible links
remove_action( 'genesis_entry_content', 'genesis_do_post_content_nav', 12 );
add_action( 'genesis_entry_content', 'child_do_post_content_nav', 12 );
function child_do_post_content_nav() {
	wp_link_pages( array(
		'before' => genesis_markup( array(
				'html5'   => '<div %s>',
				'xhtml'   => '<p class="pages">',
				'context' => 'entry-pagination',
				'echo'    => false,
			) ),
		'after'  => genesis_html5() ? '</div>' : '</p>',
		'link_before' => '<span>',
		'link_after'  => '</span>',
		'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'genesis' ) . '</span> %',
		'separator'   => '<span class="screen-reader-text">, </span>',
	) );
}


/**
 * Stop Genesis archives from using first attached image as fallback when no featured image is set.
 *
 * @param  array $args Default image arguments.
 *
 * @return array       Amended default image arguments.
 */
add_filter( 'genesis_get_image_default_args', 'prefix_stop_auto_featured_image' );
function prefix_stop_auto_featured_image( $args ) {
	if ( ! isset( $args['context'] ) || 'archive' !== $args['context'] )
		return $args;
	$args['fallback'] = false;
	return $args;
}

//* Add custom season dependent body class Not Working?
add_filter( 'body_class', 'pnet2016_add_season_body_class' );
function pnet2016_add_season_body_class( $classes ) {

	// Get the current month as integer 1 - 12
	$m = idate('m');
	
	switch ($m) {

		// Dec - Feb
		case 12:
		case 1:
		case 2:
			$classes[] = 'winter'; // at end of array
			// array_unshift($classes , 'winter'); // at beginning of array
			break;

		// Mar - May
		case 3:
		case 4:
		case 5:
			$classes[] = 'spring';
			break;

		// Jun - Aug
		case 6:
		case 7:
		case 8:
			$classes[] = 'summer';
			break;
			
		// Sep - Nov
		case 9:
		case 10:
		case 11:
			$classes[] = 'autumn';
			break;	
	}

	return $classes;
}

 
/**
 *
 */
function pnet2016_embed_defaults() {
        
    $width = 688; // (with sidebar) max content width - ( 2 x padding ) [848-(2*80)]
    
    return array(
        'width'  => $width, 
        'height' => min( ceil( $width * 1.5 ), 1000 )
    );
}
add_filter( 'embed_defaults', 'pnet2016_embed_defaults' );
