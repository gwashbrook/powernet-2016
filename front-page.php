<?php
//* Enqueue scripts
add_action( 'wp_enqueue_scripts', 'pnet2016_front_page_enqueue_scripts' );
function pnet2016_front_page_enqueue_scripts() {
	
	//* Load scripts only if custom background is being used
	if ( ! get_background_image() )
		return;

	//* Enqueue Backstretch scripts
	wp_enqueue_script( 'pnet2016-backstretch', get_bloginfo( 'stylesheet_directory' ) . '/js/jquery.backstretch.min.js', array( 'jquery' ), '1.0.0' );
	wp_enqueue_script( 'pnet2016-backstretch-set', get_bloginfo('stylesheet_directory').'/js/backstretch-set.js' , array( 'jquery', 'pnet2016-backstretch' ), '1.0.0' );

	// wp_localize_script( 'pnet2016-backstretch-set', 'BackStretchImg', array( 'src' => str_replace( 'http:', '', get_background_image() ) ) );
	
	wp_localize_script( 'pnet2016-backstretch-set', 'BackStretchImg', array( 'src' => str_replace( 'http:', '', pnet2016_get_background_image() ) ) );

	
}

function pnet2016_get_background_image() {



	
	// Returns featured image of most recent blog post if it exists
	$args = array ( 'numberposts' => 1 );
	$recent = wp_get_recent_posts( $args, OBJECT );
	wp_reset_query();

	if ( $recent ) {
		if ( has_post_thumbnail( $recent[0] ) ) {
			return get_the_post_thumbnail_url( $recent[0], 'full');
		}
	}
	/**/
	
	/*
	// An array of background images
	$bg_images = array (
		'https://my.powerhut.net/wp-content/uploads/2017/06/summer-field.jpg', // 0 January
		'https://my.powerhut.net/wp-content/uploads/2017/06/summer-field.jpg', // 1 February
		'https://my.powerhut.net/wp-content/uploads/2017/06/summer-field.jpg', // 2 March
		'https://my.powerhut.net/wp-content/uploads/2017/06/summer-field.jpg',
		'https://my.powerhut.net/wp-content/uploads/2017/06/summer-field.jpg',
		'https://my.powerhut.net/wp-content/uploads/2017/06/summer-field.jpg',
		'https://my.powerhut.net/wp-content/uploads/2017/06/summer-field.jpg',
		'https://my.powerhut.net/wp-content/uploads/2017/06/summer-field.jpg',
		'https://my.powerhut.net/wp-content/uploads/2017/06/summer-field.jpg',
		'https://my.powerhut.net/wp-content/uploads/2017/06/summer-field.jpg',
		'https://my.powerhut.net/wp-content/uploads/2017/06/summer-field.jpg',
		'https://my.powerhut.net/wp-content/uploads/2017/06/summer-field.jpg', // 11
	
	);
	
	
	// http://php.net/manual/en/function.idate.php
	
	// Get current month as integer (0 - 11)
	$month_int = idate('n') ;
	
	// if( $month_int == 11 ) {
		// return $bg_images[$month_int-1];
	// }
*/

	return get_background_image();

}



//* Add custom body class
add_filter( 'body_class', 'pnet2016_add_body_class' );
function pnet2016_add_body_class( $classes ) {
	$classes[] = 'pnet2016-frontpage';
		return $classes;
}


// WIDGETS
//* Add widget support for homepage if widgets are being used
// add_action( 'genesis_meta', 'pnet2016_front_page_genesis_meta' );
function pnet2016_front_page_genesis_meta() {

	if ( is_active_sidebar( 'home-featured-1' ) || is_active_sidebar( 'home-featured-2' ) || is_active_sidebar( 'home-featured-3' ) ) {

		//* Add Home featured Widget areas
		add_action( 'genesis_before_content_sidebar_wrap', 'pnet2016_home_featured', 15 );

	}
}

//* Add markup for homepage widgets
function pnet2016_home_featured() {

	printf( '<div %s>', genesis_attr( 'home-featured' ) );
	genesis_structural_wrap( 'home-featured' );

		genesis_widget_area( 'home-featured-1', array(
			'before' => '<div class="home-featured-1 widget-area">',
			'after'	 => '</div>',
		) );

		genesis_widget_area( 'home-featured-2', array(
			'before' => '<div class="home-featured-2 widget-area">',
			'after'	 => '</div>',
		) );

		genesis_widget_area( 'home-featured-3', array(
			'before' => '<div class="home-featured-3 widget-area">',
			'after'	 => '</div>',
		) );

	genesis_structural_wrap( 'home-featured', 'close' );
	echo '</div>'; //* end .home-featured

}


//* Run the Genesis loop
genesis();
